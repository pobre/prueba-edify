import React, { useEffect, useState } from 'react'
import { handleEstateRequest, handleProfileRequest } from '../components/services/handler'
import GridView from '../components/map/GridView'
import Layout from '../components/Layout'

const Guardadas = () => {
    const [estates, setEstates] = useState([])
    const [user, setUser] = useState({})

    const handleGetEstate = () =>{
        let likedEstates = JSON.parse(localStorage.getItem('liked_estates'))
        setEstates(handleEstateRequest().filter(e => likedEstates?.includes(e.id)))
    }

    const handleGetProfile = () =>{
        setUser(handleProfileRequest())
    }
    
      useEffect(() => {
        handleGetEstate()
        handleGetProfile()
      }, [])


    return (
        <Layout user={user}>
            <GridView estates={estates} />
        </Layout>
    );
}

export default Guardadas;