import React, { useEffect, useState } from 'react'
import { Image2, Category } from 'react-iconly'
import Layout from '../components/Layout'
import { handleEstateRequest, handleProfileRequest } from '../components/services/handler'
import GridView from '../components/map/GridView'
import MapView from '../components/map/MapView'
import Header from '../components/Header'
import Switch from '../components/Switch'

const SWITCH = [
  {
    id: 1,
    name: "grid",
    icon: Category,
    Component: GridView
  },
  {
    id: 2,
    name: "map",
    icon: Image2,
    Component: MapView
  }
]

export default function Home() {

  const [estates, setEstates] = useState([])
  const [user, setUser] = useState({})

  const [switchActive, setSwitchActive] = useState(1)

  const handleGetProfile = () =>{
    setUser(handleProfileRequest())
  }

  const handleGetEstate = () =>{
    setEstates(handleEstateRequest())
  }

  useEffect(() => {
    handleGetEstate()
    handleGetProfile()
  }, [])


  return (
    <Layout user={user}>
      <div className="space-y-10">
        <Header profile={user} />
        <Switch active={switchActive} setActive={setSwitchActive} tabs={SWITCH} />
        {
          SWITCH.map((tab)=>{
            if(switchActive === tab.id ){
              return (
                <tab.Component estates={estates} />
              )
            }
          })
        }
      </div>
    </Layout>
  )
}
