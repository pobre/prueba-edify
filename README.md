
# Prueba Técnica Edify

Aquí os explicaré como he estructurado el proyecto y qué tecnologías he utilizado.

## Arranque del proyecto

 - git clone https://gitlab.com/pobre/prueba-edify.git .
 - npm install
 - npm run dev

## Tecnología

Para realizar este mini proyecto, he optado por usar:


**NextJS** - Uno de los mejores frameworks alternativos para React según mi opinión ya que nos facilita la forma de organizar el enrutado del proyecto y da una mejor libertad a la hora de gestionar las vistas. *(ademas de que es Server Side Rendering 😉)*

**Tailwindcss** - Como punto de apoyo en el css he utilizado uno de los frameworks mejor valorados hasta la fecha como lo es Tailwind, el hecho de estar escribiendo código html a la vez que las etiquetas css hacen que la escritura del código sea más fluida. Además de esto, he personalizado el proyecto adaptando los colores de la compañía desde el fichero *"tailwind.config.js"* que se encuentra en la carpeta raíz del proyecto.

**Elementos secundarios:**

- **Leaflet:** Mapa que he utilizado para el proyecto. 

- **Framer Motion:** Para realizar algún efecto dentro de la aplicación como el despliegue del *Navbar* cuando usas la versión *movil* 📱

## Estructura

📁**components**
 -

Aquí se encuentran todos los componentes del proyecto

 - 📄**Layout.js** Este es el fichero principal que se encarga de organizar el proyecto de manera interna. Aquí se encuentra el sidebar, navbar y el body principal de la aplicación.

 - 📁**services** Esta es la carpeta que simularía tener los ficheros de rutas para hacer las llamadas al API.

 - 📁**nav** Aquí encontramos los distintos navegadores de la aplicación, como el *sidebar.js* que se muestra cuando la aplicación es abierta desde un dispositivo tablet/pc, y *navbar.js* cuando es abierta desde un dispositivo movil.

 - 📁**Switch** Este es el componente que uso para realizar el cambio de pestaña entre la vista de *GRID* y *MAP* en las propiedades que se muestran

 - 📁**map** En esta carpeta encontramos las 2 vistas principales de la aplicación, *GRID* y *MAP*. Para no hacer los archivos tan largos, divido la funcionalidad de las vistas del HTML y los personalizo creando Hooks que luego reutilizo dentro de la propia aplicación como *useLikeEstate* que sirve para guardar y obtener las propiedades. *( usando el localStorage como gestor de datos )*

 - 📁**hooks** Aquí guardo todos los hooks personalizados que se puede usar por toda la aplicación, en este caso guardo *useThousand.js* que sirve para formatear un número con el punto que suelen llevar los miles (ej: 10000 -> 10.000)

 - 📁**Header** Aquí guardaría el componente Header que es donde recojo el nombre del perfil y su avatar, además del presupuesto.


📁**styles**
 -

 - 📄**global.css** En este archivo guardo las fuentes de texto que aplico en la aplicación ademas de la modificación de la barra de scroll y el contenedor del mapa.

📁**pages**
 -

Aquí se encuentran todos los archivos que hacen de ruta del proyecto

 -📄**index.js** Este sería el archivo principal, la ruta '/' donde consumo los componentes de *Todas las propiedades*

 -📄**guardadas.js** Este archivo es en sí el componente de la ruta '/guardadas' donde rehuso el componente *GridView.js* de la carpeta *map* para mostrar solo las guardadas.

📁**public**
 -

Aquí se almacenan todas las resources del proyecto en sí.

- 📁**fonts** Todas las fuentes de letra del proyecto

- 📁**img** Todas las imagenes divididas por subcarpetas a su vez.

- 📄**favicon.ico** El favicon de la app
