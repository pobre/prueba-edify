module.exports = {
  mode: 'jit',
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    zIndex: {
      'max': 9999,
    },
    extend: {
      backgroundColor: theme => ({
        'primary-green': '#143233',
        'primary-orange': '#E16736',
        'primary-blue': '#35748E',
        'primary-pink': '#F3BEB3',
        'primary-yellow': '#EAF93B',
        'secondary-green': '#14323380',
        'secondary-grey': '#F9F9F9',
        'secondary-white': '#FFFFFF',
      }),
      borderColor: theme => ({
        'primary-green': '#143233',
        'primary-orange': '#E16736',
        'primary-blue': '#35748E',
        'primary-pink': '#F3BEB3',
        'primary-yellow': '#EAF93B',
        'secondary-green': '#14323380',
        'secondary-grey': '#F9F9F9',
        'secondary-white': '#FFFFFF',
       }),
       textColor: theme => ({
        'primary-green': '#143233',
        'primary-orange': '#E16736',
        'primary-blue': '#35748E',
        'primary-pink': '#F3BEB3',
        'primary-yellow': '#EAF93B',
        'secondary-green': '#14323380',
        'secondary-grey': '#F9F9F9',
        'secondary-white': '#FFFFFF',
       })
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
