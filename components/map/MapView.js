import React from 'react'
import useMapView from './hooks/useMapView'
import useLikeEstate from './hooks/useLikeEstate'
import useThousand from '../hooks/useThousand'
import { Heart } from 'react-iconly'
import 'leaflet/dist/leaflet.css'

const MapView = ({ estates }) => {

    const {format} = useThousand()

    const {
        map,
        selected,
        renderMap
    } = useMapView()

    const {
        liked,
        handleLike
    } = useLikeEstate()

    return (
        <div className="bg-white rounded-xl p-3 relative">
            {map && 
                renderMap({ estates })
            }
            {selected &&
                <div className="absolute w-96 p-4 top-0 right-0 lg:top-5 lg:right-5 bg-white z-max rounded-xl ">
                    <div className="w-full relative">
                        <img src={selected.image1} alt="imagen" className="w-full h-auto rounded-lg" />
                        <span className="bottom-2 right-2 bg-primary-green rounded-lg px-2 absolute text-white ">{format(selected.price)}€</span>
                    </div>
                    <div className="mt-5 px-2">
                        <div>
                            <div className="flex items-center justify-start">
                                <div className="w-2/3">
                                    <h1 className="font-semibold lg:text-md 2xl:text-lg font-tiemposhl">{selected.name}</h1>
                                </div>
                                <div className="w-1/3 flex justify-end">
                                    <span className="bg-yellow-100 text-primary-orange px-3 py-0.5 text-md rounded-lg ">{selected.height}</span>
                                </div>
                            </div>
                            <p className="truncate text-gray-500">{selected.description}</p>
                        </div>

                        <div className="mt-4 flex">
                            <div className="w-1/2 space-y-3">
                                <div className="flex items-center justify-start space-x-3">
                                    <img src="/img/house/rule.png" alt="medir" className="w-6 opacity-70" />
                                    <span className="text-gray-500 ml-4">{selected.meters}m</span>
                                </div>
                                <div className="flex items-center justify-start space-x-3">
                                    <img src="/img/house/bed.png" alt="medir" className="w-6 opacity-70" />
                                    <span className="text-gray-500">{selected.rooms}</span>
                                </div>
                            </div>
                            <div className="w-1/2 space-y-3">
                                <div className="flex items-center justify-start space-x-3">
                                    <img src="/img/house/bath.png" alt="medir" className="w-6 opacity-70" />
                                    <span className="text-gray-500 ml-4">{selected.bath}</span>
                                </div>
                                <div className="flex items-center justify-start space-x-3">
                                    <img src="/img/house/parking.png" alt="medir" className="w-6 opacity-70" />
                                    <span className="text-gray-500">{selected.Garage ? 'Si' : 'No'}</span>
                                </div>
                            </div>
                        </div>

                        <div className="mt-4">
                            <button onClick={()=>handleLike(selected)} className={` duration-300 transition-all rounded-xl justify-center items-center flex p-3 w-full ${liked?.includes(selected.id) ? 'bg-primary-orange text-white' : 'bg-secondary-grey hover:bg-gray-200'}`}>
                                <span>
                                    <Heart set={liked?.includes(selected.id) ? 'bold' : 'light'} />
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
            }
        </div>
    );
}

export default MapView;