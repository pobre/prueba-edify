import React from 'react'
import { Heart } from 'react-iconly'
import useGridView from './hooks/useGridView'
import useLikeEstate from './hooks/useLikeEstate'
import useThousand from '../hooks/useThousand'
import 'leaflet/dist/leaflet.css'

const GridView = ({ estates }) => {

    const {format} = useThousand()

    const {
        map,
        renderMap,
    } = useGridView()

    const {
        liked,
        handleLike
    } = useLikeEstate()

    return (
        <div className="grid grid-cols-1 lg:grid-cols-3 2xl:grid-cols-4 gap-8">
        {map && 
          estates?.map((st)=>{
          return(
            <div className="col-span-1 bg-white rounded-xl p-3 shadow-sm">
              <div className="w-full relative">
                {renderMap({ position: [st.lat, st.lon]})}
                <div className="bg-white rounded-bl-md p-1 absolute top-0 right-0 z-max justify-center items-center flex w-8 h-8">
                  <span className="text-primary-green ">
                    <Heart set={liked?.includes(st.id) ? 'bold' : 'light'} onClick={()=>handleLike(st)} className="cursor-pointer w-8 active:w-2 duration-200 transition-all" />
                  </span>
                </div>
                <div className="bg-primary-green rounded-md p-1 absolute bottom-2 right-2 z-max">
                  <span className="text-white">{format(st.price)}€</span>
                </div>
              </div>

              <div className="mt-5 px-2">
                <div>
                  <div className="flex items-center justify-start">
                    <div className="w-2/3">
                      <h1 className="font-semibold lg:text-md 2xl:text-lg font-tiemposhl">{st.name}</h1>
                    </div>
                    <div className="w-1/3 flex justify-end">
                      <span className="bg-yellow-100 text-primary-orange px-3 py-0.5 text-md rounded-lg ">{st.height}</span>
                    </div>
                  </div>
                  <p className="truncate text-gray-500">{st.description}</p>
                </div>

                <div className="mt-4 flex">
                  <div className="w-1/2 space-y-3">
                    <div className="flex items-center justify-start space-x-3">
                      <img src="/img/house/rule.png" alt="medir" className="w-6 opacity-70" />
                      <span className="text-gray-500 ml-4">{st.meters}m</span>
                    </div>
                    <div className="flex items-center justify-start space-x-3">
                      <img src="/img/house/bed.png" alt="medir" className="w-6 opacity-70" />
                      <span className="text-gray-500">{st.rooms}</span>
                    </div>
                  </div>
                  <div className="w-1/2 space-y-3">
                    <div className="flex items-center justify-start space-x-3">
                      <img src="/img/house/bath.png" alt="medir" className="w-6 opacity-70" />
                      <span className="text-gray-500 ml-4">{st.bath}</span>
                    </div>
                    <div className="flex items-center justify-start space-x-3">
                      <img src="/img/house/parking.png" alt="medir" className="w-6 opacity-70" />
                      <span className="text-gray-500">{st.Garage ? 'Si' : 'No'}</span>
                    </div>
                  </div>
                </div>

                <div className="mt-4">
                  <span className="text-sm text-gray-400">{new Date(st.createdAt).toLocaleDateString("es-ES", { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' })}</span>
                </div>
              </div>
            </div>
          )
        })}
      </div>
    );
}

export default GridView;