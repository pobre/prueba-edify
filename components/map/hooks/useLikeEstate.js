import React, {useState, useEffect} from 'react'

const useLikeEstate = () => {
    const [liked, setLiked] = useState([])

    const handleLike = (e) =>{
        let newLike = liked.map(l => l)
        if(liked?.includes(e.id)){
            newLike = liked.filter(l => l !== e.id)
            setLiked(newLike)
        }else{
            newLike.push(e.id)
            setLiked(newLike)
        }
        localStorage.setItem('liked_estates', JSON.stringify(newLike))
    }

    useEffect(() => {
        let newLike = JSON.parse(localStorage.getItem('liked_estates'))
        if(newLike !== undefined && newLike !== null){
            setLiked(newLike)
        }
    }, [])

    return {
        liked,
        handleLike
    }
}

export default useLikeEstate;