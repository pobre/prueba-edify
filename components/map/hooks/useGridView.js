import React, {useEffect, useState} from 'react'

const useGridView = () => {

    const [map, setMap] = useState(false)
    
    const renderMap = ({ position }) =>{
      const {MapContainer, Marker, TileLayer} = require('react-leaflet')
      const {Icon} = require('leaflet');
      return (
        <MapContainer className="preview" center={position} zoom={14} scrollWheelZoom={true}>
          <TileLayer 
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" 
            attribution='<a href="http://osm.org/copyright"></a>'
          />
          <Marker icon={new Icon({iconUrl: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAB2AAAAdgB+lymcgAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAXlSURBVHic7ZpbaBxVGMd/38zsZnPZmGbbkOINpV7Bhz5UH0Qs4oviXaPVFBGtoVWq4IsgXoKooOANtCbRIoJmU1YUEcEXsVoVL6AiUkK9YLW2adotqZtkN7sz8/mQRlttmzkzZ9cH9/f8ff/znf+e2TPnOwNNmjRp0uT/izRiEB0YSPkl5yLgEuBc4GygF2g/FDIDTADjwHbE+cDb3bNNtg769a6trgbM9Q+c5wbuvSp6A3CCWbZOCfJmEPJ8y5ah7+tSIHUyoLJ2w1luoM8Al1uQU5T3Akfvy4wO/2BB7wisGqCrBz2/d+JhhPuBtE1tYA7kSc/f/6gUCoEtUWsGaN/GZb5XGwUutaV5DD72fOcmKWyasCFmxQDtGzglcN2tKnqaDb3FEPjJdfzV8sYruyxoJUP7150UhN5HCqcn1TJBlB/dgNVSGPo9iY6TJFkv29jiB95bjZ48gAorfI+39LKNLUl0EhkQLPGfQ1iVRCMh5wdLqk8nEYj9CNT6119IyLYkGpZQRFenRoc/jpMcq3jt63N9L/cVsNIoT2GmVGa2NEtlrkZQm9/NXM8lk0nTlm2lPZtBxLisbzy/uCrO9uiZJgAEbu4qDCc/XSpTnDyIX/33221Y9alVfUp/zJJKe+R6TqA922oivzJIdV8BvGOSBDH/A1TYYBJf3HuQvbuKR538P6lVfSZ2FSlOHgQ1qEmd9SY1LWC81ipr71rhBuGOqLnFvQeZOlAyLgygK5cl1xP5CBEGImdkRl/62WQM4xXgBnotESc/XSrHnjzAVLHETKkcNdxxQr3adIwYj0B4cZQo1flfPyn7J6fQiI+CQKTaDsfIAO3rc4ELo8TOlMr4teTHeb8aMFOajRYsXKSDg0ZzMgquuLmTQbqixBos3UWZnZ6LGtrN+OSJJtpGBniiy6PGzlWqJtLHpVKObAA1pddE28gAESIbEPjWjuxGWo4T1M8A1GkzireGwW4t0r540N+YGSBh5D3N9Vwj6eNrGZQZitG+a7YLaHTxTCZlIn18rdboJ151ov9IYGhAiLs3amxb1t7T0pbNRI4NA3efibaRAWl/chwh0qbcns2QSsc6ax2Bl/Zo74h8MJpOh/t2mOib7QKFQkDId5FiReheZngVcBSW9nQR/XQs35oeic1fhYVPo4Z2dLbS1d1hPMQCXUs7aTdY/qCfmY5hbICGzhaT+FxPF125rOkw8yfBpZ1GOaqMmY4TqyNUu3n9OHCWSc7MoYZIbZGewHxDpMvwlwdgPJUfOsc0Kea/lGwGfcokoz3bSltHZr4lNl2hUq7+9Ybnei6Z1kMtsY5YLTFAX46RFM8Az8+84Hvle4CTTPJEhI7ONjo6rb9Q/u5N61CcxFgtMSk8W1Z4LE5uPVDRR+TdkYhn5iOJfS+Q2tO7GeXzuPkW+Sy1e/lrcZMT9fR1zV0n+xJ+A+SS6CSoYMpXWdk6NvRLXIVEN0Mytuk3Ue7EqH9rDRWV25NMHhIaAOCNDb0NPJFUxxjRxw+NnYjEBgB4+aGHgIINrUgo73hnLn/EhpQVAwTU853bIdo5ISHbPTd1qwwOhjbE7H4ic8vdp/oafAUss6l7GMUgDC/IbBn5yZaglRWwgIy+uBPC6wB7HdG/qSFOn83Jg2UDAFL5kU8UiXVPdzxU9Z7U6KYPbetaNwAgnX/pVYFNtvRE9MX02HCsV93FqIsBAO6e3nsRPrAgtc2tHbjPgs5RqevXHdp3R3fgpr5QYUWsfPglRbhK8iP7bde2QN1WAIAUNh8IJLwSiHNLWtKQK+s5eaizAQAt+ZFx0DWASa8uFLS/nt8IL1B3AwBS+eH3gQcjJ4g84OWH361fRYcN1YhBFqjevP51gf5FwgpefugmadABqyErYIFUS2UdyBfHDBC+9qbD2xo1+fkhG4yuHVjuB86X/LudNuE5/iob3/+a0NAVACCvj+zRMLz6HzdMFdXwmkZPHv4DAwDSW0a+llBuVOFXhZ0ien16bOTYj0aTJk2aNGlSF/4EXer2Pm9N/SwAAAAASUVORK5CYII=', iconSize: [64, 64]})} position={position} />
        </MapContainer>
      )
    }
  
    useEffect(() => {
      if(document){
        setMap(true)
      }
    }, [])

    
    return {
        map,
        renderMap,
    };
}

export default useGridView;