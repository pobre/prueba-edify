import React, {useEffect, useState} from 'react'

const useMapView = () => {

    const [map, setMap] = useState(false)

    const [selected, setSelected] = useState(false)
    
    const renderMap = ({ estates }) =>{
        const {MapContainer, Marker, TileLayer, Polygon} = require('react-leaflet')
        const {Icon} = require('leaflet');
        const colorBlue = { color: 'orange' }
        const polygonArray = [
            [39.449558, -0.382478],
            [39.474596, -0.333983],
            [39.487921, -0.377846],
            [39.472744, -0.408746]
        ]
        return (
            <MapContainer 
                center={[estates[0].lat, estates[0].lon]} 
                zoom={13} 
                scrollWheelZoom={true}
            >
                <TileLayer 
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" 
                    attribution='<a href="http://osm.org/copyright"></a>'
                />
                <Polygon pathOptions={colorBlue} positions={polygonArray} />
                {estates.map((estate)=>{
                    return(
                        <Marker 
                            eventHandlers={{
                                click: () => setSelected(estate)
                            }}
                            icon={new Icon({iconUrl: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANkAAADZCAYAAACtvpV2AAAAAXNSR0IArs4c6QAAG9JJREFUeF7tXQnUXtO5fl7jqsYU1kKpoq4pERLXfGsoEleXecpVRdCuGtKa5wpKDTEWZamEoG7EGNZ1JaFCryluEyIRXEWVYi0JIdUlhveuJ9/++ST/931n2vvsc773XeusL1k5e7/vfvZ5ss/Zw/MKzAwBQ8ArAuK1dqvcEDAEYCSzh8AQ8IyAkcwzwFa9IWAks2fAEPCMgJHMM8DtqlfV5QD0BbCSu3r+3Ol3NoA5AJL8LrhHRD4qsald7dpIFqD7VfXbADYCMADApk1XnwDue1zMA/Bc0zUdwIsi8o+AMXSlKyNZwd2uqms5QvUD0N8RiuSK1Ug2km8GgJmOeG/EGmwV4zKS5ew1Vd0ZwGAAJBWv7+WsMobif3WEI+kmisjDMQRV1RiMZBl6TlX5yrcHgD0BDMpQRdWKTAUwHsD9IsJRzywFAkayhGCp6uqOVCTXkITF6njbBJKNpBORt+vYwKLbZCRrg6iqLuWIxRGLV8iJiqL7uuj6OJHC0W3BJSLzi3ZQl/qMZL30pKru0ESutevS2R7b8XoT2SZ79FPJqo1krtvcqHUEgMMAbFHJ3owj6CkAbgYwyka3Rod0PclUdXkAJNfhbnYwjke1+lFwZnK0I9vc6jcnewu6lmSquqojFgm2TnYIrWQHBF4j0Ug4EXm3G9HqOpK5xeKekes73djpJbX5700jW1ctdncNyVR1w6aRa8WSHjRzC3zQNLLN6gZAak8yVR3Y9M31rW7o1Iq08Z9NI9u0isScKcxak0xVfwXgvEzIWKGQCJwtIr8O6TCkr1qSTFW3cuTaJSSY5isXApMAkGxP56olwsK1I5mqngzgXAD2ahjhA9chJL5CjhCRkdULvXXEtSGZ27RLcnFvoVm1EeDeSJKtFpuRa0EyVT3WvR7arGG1ydUcPWch+fp4TdWbVGmSqer6jlwHVL0jLP6WCIxzZHu5qhhVlmSqygVlzhzagnJVn77kcXMhm6Mad45UzipHMlVdFsDVAA6tHNoWcF4ExgAYLiIf560oZPlKkUxVNwBwPYDtQ4JkvqJC4DEAPxeRl6KKqk0wlSGZqu7kCLZuVcC1OL0h8Koj2iPePBRYcSVIpqqHOILZ2leBnV/xqrimxhHtltjbET3JVPV0AL+JHcjM8c2bB3z4IfDBB43fnqvd31dYAeC14oqN305/71Nr1YQzROTCzPgHKBg1yVSVayTHBMAhjIv584Hnnweee67xy4sk820k2SabNK5NN238LkX5ktrYtSLCtdIoLVqSqeo9APaOErWkQb33XoNIL7zQuGbMAL74Imlpf/ctvjjQvz+w8caNi6RbZRV//sLUfK+I7BPGVTovUZJMVZ+ppM7GO+8AzzwDzJzZINXLFVo/XX/9BuH69QO23BJYbbV0T1Icd08RkS3jCOXrKKIjmaq+BYAah9Wxp54CJk5sXJ98Up24W0W6zDLA4MGNa+utq9aet0VkjZiCjopkqqoxgdM2ljlzvibWtBqfORw48GvC9WWymWqYiETzbEcTiKq+CIASAXEbCdUzapFo3WIkWM/oRuLFb7NEhJl0SrcoSKaqdwHYt3Q0WgXAV8AeYvHVsNuNr5A9hOOrZbx2t4jsV3Z4pZNMVXns/KyygWjp/5ZbgLFjAU5qmH0TAU6ODB0KHMK9AtHa+SJCGYrSrFSSqeowJ6ZSGgAtHXPEuukm4Nln44sttog23xwYNizmSZLDReSmsmArjWSqygQO95XV8JZ+Z88GRo8Gbr89utCiD+igg4DDDwdWYnbe6GwvEWFyjOBWCslUdTsA3E0dl40f3yDYm2/GFVeVollzzQbR9uT/odHZ9iLyeOiogpNMVZni9QkATEoeh82aBYwaBTxSiU3dcWDWKYqddgKOOALYMKoJYyan31ZEmLo3mAUlmaryFDNfETcP1sJ2jj7/vPHdRYJ9+mkUIdUqiKWXbhCN32tLLBFL0/iRzVdHnrYOYsFIpqo8pkKCMb9y+TZ5cuPVkNufzPwiwO1afIXcgWnforCJjmg8LuPdQpKMOavikAy4+27g/PO9g2sOFkLgrLOAfaNZDh0jIsxF592CkMyJ3tzovTVJHHD0upoSIWalIDB8eGNUi8OODCHO451kTrbtj1GoSl1xBcDFZbNyEeDi9fHHlxtDwzu/y34oIl6PS4Qg2R0AytdFPPNM4MEHY+hYi4EI7LYbcMEFMWAxTkQO9BmIV5I5Zd9y383mzgV+9jPglVd84mh1Z0FgvfWAG24AlmdG4VKNMnPelIq9kcxp0/M1sTzpbC4q77NPHKeRS32GInbOU9r33ANwEbs8oyQ4Xxu9aO/7JBm3sJSX/GHq1MYajVk1EOBa5aBBZcZ6v4h42abihWQufdElpSE2bhxwYdQCRqVBE7Xj008HDij18/0UH2mbCieZS8DH18RyNBJvuw247LKonyULrg0CJ54IHHxwWRBxcZqvjYUmIvRBMq6ml5PhctIk4JRTyuog81sUApdcAuxSziMEYJKIFLorqVCSlZqjefp04NA4NpQU9ax1dT1jxgADBpQFQaE5rAsjmapS+GFqKahQ35DrLl9+WYp7c+oBgcUWa6xrlqcHOUhEClFIKpJk5an9kmAmD+DhSS+5SsoblLeBoDBV4kJIpqo8NPTnUiY7eIyCstdm9USAsuI8jhTeOAmymYjMyuu6KJIxW/1JeYNJXf6004AJE1IXswIVQ2DIEOCii8oI+lIROTmv49wkU9W13LdY2J0dttk3b99Xq3w5m4q5E4TfZm/kAasIkoWXdKPIzUgOnmZdhcDJJwMU6wlruSXlcpFMVVd132LhkqPzRHMcxyTCdrV5ayDAN5iwJ6x5HIbfZu9m7YK8JDsDQLjzCtTk4IE/kwzI2t/VL0cpAx68DasZcqaIZE5EmZlkqsrzCVwXWydYz/3+98DvfhfMnTmKFIGjjwZ++tOQwb3mvs3mZnGah2QnAAi3SZCybZyuN1WpLP1crzJUweK0fli5uRNF5PIsQGYimaoyFypHsX5ZnGYqc9JJpouYCbiaFqKu46WXhmzcTDeazU/rNCvJjgIQ7r2Nyr7nnJO2bXZ/3RHgMxFWqfhoEbkuLaxZSRYu3Sy16TnZYdLZafu2/vfzNDUnQcJp72dKl5uaZKpKhcpHg/Ug18Ms+UMwuCvniOtmXD8LZzuKyOQ07rKQ7AoAx6Vxkvlepi/iTJKZIdAOAc44h8ttfaWIpNKzS0UyN+HxEoC1g/Q6VaYsP1gQqCvthPnRqHoVxl4HsIGIJJ4ASUuy/QGMC9IWipBydd/MEEiCAHcBhcv4eYCI3JkkLN6TlmS3Afhx0soz38cczfvtZ2fEMgPYhQV59uyuu4AwOaz/ICKJhUgSk0xVVwfAV8U+3rvwvvuAc8/17sYc1AyBESOAvfYK0ah57pXx7STO0pCMMxDXJqk09z2c7OCkh5khkAYBTn6E23Z3jIgkWitOQ7KHAAxJ0+ZM906bFlPWj0xNsEIlIsB1s4GUm/FuE0Rk1yReEpHMSW4XIirSMaiLLwbGju14m91gCPSKwNChwKmnhgJnYBJp76QkOxuA/4+kOXOA/fcH+GtmCGRBoG9f4M47Af76txEicl4nN0lJRpEc/0LlHME4kpkZAnkQ4EjGEc2/TRWRzTq56UgyVd2ZqqqdKirk37lHkd9kZoZAHgT4TcZvszC2i4g83M5VEpIxcYT/zWG2hSrMI9EtXsJttRopIm214ZOQjOkp/91733BdjOtjZoZAEQhwvYzrZv7tv0Vkt7wj2ZsAvus1Vqr/cocHd3qYGQJFIMCdH9wBwp0gfu1vItI2g2HbkUxVvw/gVb8xArBDmd4h7koH4Q51risif2mFcSeS7Q3gHu8dxIR9TNxnZggUiQATCjKxoH/bR0TuzUqyXwHouA6Quw0/+QkwY0buaqwCQ+AbCPTvD9x6awhQ2qZa6jSS3QHAb37R998vM+FbiA4wH2UiwMSQK6/sO4JxInJg1pGMCj0beY3wsceA48IctPbaDqs8TgSuvBLYfnvfsb0oIi2V21qOZKrKIy0f+45uwa5pipaaGQI+EKAIahgJi2VFhEdgFrF2JNsGwBM+2v2NOk1iwDvEXe0gnDTBtiLyZFqS/RxAao25VB362WcNAZQvvkhVzG42BBIjsPjijbOJSy6ZuEjGG48SkevTkowEI9H82fPPA4cd5q9+q9kQIAI33wxssolvLK4XEYr+pnpd5KsiXxn9GRt/1VX+6reaDQEi8MtfhvjP/EkR2TYtyTjp4VfPg41//HF7EAwBvwhst12I/8zniciyiUmmqssByJQmJhVaP/gBMK/XCZlU1djNhkBbBPr0Af70pxAgLS8iHy3sqNfZRZcHmiKO/ozkIsnMDIEQCJBkJJtfW7u3/NKtSMbTnv/rNZ633gJ2392rC6vcEPgKgQceANZYwzcg/yoiVBH4hrUi2WAAE7xGxJS04RRfvTbFKq8AAlSkZipcvzZERCYmJRkFEv7Tazwcvn/xC68urHJD4CsEfvvbEJ8n/yEii0ittRrJjgFwjdcu4vB9NkWwzAyBAAicd16Iz5NjRWQRAeBWJPN/xIVHEC7PlII3QI+Yi9ohcMIJAI9U+bVej7y0ItmVXMLzGg+HbybXNjMEQiAwbFiIz5OrRGSRIyWtSHYLAL+05/B9b8vDpCFgNx/dhMDee4f4PLlVRA5JOvHxXwDaKvDk7h8O34+Gy4qbO16roNoI7LhjiM+TB0XkR0lJxpQqW3lF1YRMvcJrlS+EQBjB06dFZOukJHsFwL947SgO32+84dWFVW4IfIXAWmuF+Dz5PxFZLynJ3gewktcu4vD94YdeXVjlhsBXCKywQojPk9kisoigSKuJD/XePWFySHlvhjmoEAIB8iyIyCKcakUyG8kq9OxYqAkQiHAks2+yBP1mt1QIgQi/yWx2sULPj4WaAIEIZxdtnSxBv9ktFUIgwnUy2/FRoefHQk2AQIQ7PmzvYoJ+s1sqhECEexdtF36Fnh8LNQECEe7Ct/NkCfrNbqkQAhGeJ7OT0RV6fizUBAhEeDLaND4S9JvdUiEEItT4MLWqCj0/FmoCBCJUq1oLgOkuJug7u6UiCESou2gKwhV5dizMBAjEqCDMsFXVvxb+sccCT/hPgZagG+yWOiOw7bbANX7F1wCk08J3JKN4+L95xZ4ZNplp08wQ8IkAM20y46Zf+x8R6VV3vl2mzasBHOs1rmefBZhp08wQ8InADTcAzLjp164RkeG9uWhHsiMA3Og1rvnzgW22sUybXkHu8sqZafPJJ4GllvINxJEiMiotyUj9Kb4jW5Bpkxk3zQwBHwgwwyaTTfq3LUTk2bQk+zY/5rzHdtllwG23eXdjDroUgYMPBk48MUTj+4jIP1KRjDerKoeYAV4jnDgROPVUry6s8i5G4OKLgcHcwOTVpotIy6TULb/JHMnGAFhEEbXQcN97D9h110KrtMoMga8QeOghYJVVfANyi4gc2spJJ5KdDOAS3xFi6FDg5Ze9uzEHXYbA+usDYxfJZOQDhFNEZGRWklGqm1IEfu2CC4C77vLrw2rvPgT22w8488wQ7f6RiDyYlWT+9zAysvvuA849NwQY5qObEBgxAthrrxAt7jVXdI/jtq+L7ruMWtrf8xrpO+8A/F/nk0+8urHKuwiBZZZpvB2ttprvRv9VRDgYtbQkJPOvXMXwOJJxRDMzBIpAgCMYRzL/1msml2a3SUjGiQ9OgPi1p54CuMfMzBAoAgHuid16kQQrRdS8cB0jReSUvCPZzgAm+YhukTotnVIQmGvvJIyQaQ+Mu4jIw7lIxsKq+mcAg7x3DqdbuXhoZgjkQYCbG7gs5N+mighVBNpax9dFR7Kz+dXUqbLc/z5nDrD//gB/zQyBLAj07QvceSfAX/82QkTO6+QmKck2BTCtU2WF/DtHsjALiIWEa5VEhgBHsHDb9AaKyHOdEEhEMjeaPQRgSKcKc/87c0jx28zMEMiCwOjRQJjcdxNEJNF+wDQk49TftVnanboMZxk522hmCKRBgLOJ4U7aHyMiiY71pyHZ6gBeAtAnTbsz3Ws7QDLB1vWFwu3w4BGwDUTk7SSYJyaZe2Xkwa8fJ6k41z3c+cEdINwJYmYIJEGAOzu4w4M7PfzbH0Tk4KRu0pJsfwDjklae6z4qvl5xRa4qrHAXIXD88cAhfk9lNaF5gIjcmRTdtCSjUAJfGddO6iDXfRTZodiOmSHQDgGK5FAsJ4xR9JevivOTuktFMvfKyOHluKQOct1nW61ywdc1hcNtoSKkV4rI8WmwzUKyHQA8msZJrntHjgRuvz1XFVa4xggcdBBwsv+ttU0I7igik9MgmppkbjR7BsAWaRxlvnf27Ma62ZtvZq7CCtYUgTXXBLguttJKoRo4RUS2TOssK8mOApBojSBtQL3eP348cM45hVRlldQIAT4Te+4ZskFHi8h1aR1mJRknQKYC6JfWYeb7TzoJeOSRzMWtYM0Q2Gkn4NJLQzZqJjfJp5nw6AkuE8ncK+MJAC4L1spZswAm1/7002AuzVGkCCy9NHDTTcCGG4YM8EQRuTyLwzwkW96NZutkcZypjCWoyARb7QqFSSDRDNtrbhSbmwXLzCRzo9kZAC7I4jhTmc8/b0yCvPBCpuJWqAYIbLxxY7JjiSVCNuZMEflNVod5SbYqAB7o/E7WAFKXmzwZ4Oq+WXciwF1AO3AVKZj9HcBmIvJuVo+5SOZGs18DOCtrAJnK3X03cP75mYpaoQojcNZZwL77hm7A+SLyqzxOiyAZ5bA407hinkBSl+Urw9VMoWbWFQgMH17GOcMP3LcYZREzW26SudGMEsUnZY4ia0G+OnAjsVm9EeDG33I+ES4VkdzbSYoiGedS+W32reC9TRnmB1sqJAcPxxwWjMBuuwGUcQ9v/3TfYrPyui6EZG40Y+brY/IGlKn8gQcCr7ySqagVihiB9dYD7rijrACvFZFC0jkXSbKB7tssPChz5wLcAfDFF+F9m0c/CDANLXf4LM/l2FKMuzsKEY8qjGRuNOMsTEeJLC+QcQNx2H1sXpphlToEuF+VG4DLsbNFhLPmhVihJHNEmwhgl0KiS1vJ1KnAEcwnb1ZpBEaNAgb519JtgdEkESk0NacPkm0F4I+lTIIQtXHjgAsvrPQz1tXBn346cMABZUHAyY4fisjTRQZQOMncaBYmQ2crJJjonQnfzaqFABOoM5F6edY2Y2bWsLyQzBFtPIA9sgaWu9ykScApbZNt5HZhFRSIwCWXALuU85XhWnG/iHg5nOaTZJT25mtj2J0gzf0+fXrjeMyXXxb4NFhVhSKw2GKNYysDBhRabcrKuLODr4kdJbdT1rvgdm8kc6MZ1xnK3fv03nsNopmGY5bnw28ZaiWSYKus4tdP59qHiwjXeb2YV5I5onE1sbQv2a9QI9Ge8/IflZeOqX2lm27aIFj5Nk5EDvQZRgiSre9eG8Mdh2mF2GmnARMm+MTT6k6CwJAhwEUXJbnT9z08xsLXxJd9OvJOMjeacfHqRp8NSVy3bSpODJWXG8vb7Ntbc44UkVFe2tlUaRCSOaLdDOBQ3w1KVD91HKnnaBYWAeojUicxDhsjIoeFCCUkyZYF8ACA7UM0rKMPnrDmmTSTMugIVe4bKBlA2YiwJ5rbhf0YgN1F5OPcbUtQQTCSudFsA0e0dRPE5v8Waobw45vbeEwFq3i8qSrFbW6cdAqrydGuLa86gjGnQxALSjJHtJ0c0cKfPWsFKeXmSDTTdSzuoeOpCBIsrGxbp/i5bYojWFABz+Akc0RjjpsxnRAJ/u/c+c1XSJMEzw49d87z1TDOExGHikjwo/SlkMwR7XQAmWW2sj8FHUpSe59EsyQX6SHmpAYJFk6bPk2MZ4hIKTvHSyOZI1p5p6k7dQ/TNvF7zfKjdUIKYH4wfncxZ3OcVtgp5yzNK5Vkjmj3ANg7S/BBylCoZ+xY25bVG9jcFjV0aMgMl1m6/F4R2SdLwaLKlE4yR7RwqZiyIMcc1hMnNi6OcN1uHLEGD25cYXI0Z0U8U6qjrM5alYuCZI5obwFYvegGFl7ftGlfE27OnMKrj7bCvn2/JtZAyrlEb2+LyBoxRBkNyRzRNAZQEsVAgvWMbiReXY2E6hm1SLSKmIhE82xHE0hP36nqiwCC5sTJ/dzwFbKHcHy1rLrxFbCHWPFOZrRCeZaIbBRTF0RHMjei3QUguOh57o7hmbUpU4AXX2xcM2bkrjJYBf37Axtt1Li22ALgpEb17G4R2S+2sKMkmSNa+EQWRffO++8DM2c2Lp5lo5pWDNqQ1DSkGhTPdPXr17hWXrno1oeuL3diCF8BR0syR7RhAEb7anzwej/7rDHC8RuOF0k3b57/MPr0aZCK31e8OFotuaR/v+E8HC4iUZwA7a3JUZPMEY3iJveF66/AnkiyDz8EPvig8dtztfv7CisAvFZcsfHb6e8kWX1tLxGhaFO0Fj3JHNG2c5uKl4sWSQssNAIfuc2+j4d2nNZfJUjmiNbfvTpunraRdn/tEHgWAF8RKzGzVBmSOaJRJ4Tv3oXKKNfuEax3gygDP0xEqM9RCasUyRzReA7tumikDCrRzbUJksejjhIRngurjFWOZD3IqirFeZhBpnwVrMp0d2UD5ajFTCveRW98IFRZkrlRjXJzJFr5uo4+esfqJALjHMG8yrb5hLrSJGsa1ahUTLKVJwnus5e6s25KZ3P08qbsGwrWWpDMjWrU3j+31CQXoXqt/n7uBzDClzZ9aPhqQ7KmUY1pm0i2eIR6Qvdqdf1xQoPkqpUoZu1I5kY1JiLk62OpuXiq+6yXEvkk93pYaAK+UlqykNNakqxpVCsvh3UMvVudGArN0Rxbs2tNMjeq8Rgvp/sPt1fIqB4/vhpy8/coEanxqVfP+cli6lJV5UFQEo2Es1nI8jqHs4Zc7xotIrPKCyOc59qPZAtDqaprNY1stpAd7lnjgnLPyPVGOLfle+o6kjV9r63aNLKtU35X1DaC15pGrndr28o2DetakjWRbfmmka1fNz4Ento8s2nkmuvJRyWq7XqSNZFtKUc25qzaohK9F2eQUwAwFx0nNObHGWLYqIxkveCtqjsA4IlsXmuH7ZJKensdAE8njxeRyZVsgcegjWRtwFVVjm49ZONvrc/xp3zOKE6ygFiOXDZqtQDQSJbwyVJVqhuTaHsAGJKwWB1vY2Z77i3kqPV2HRtYdJuMZBkQVVVuRibZSLpBGaqoWpGpbsS6vy6bdkN2gJEsJ9qqurOTQ6AGCa/v5qwyhuJ/A0D9DF4TReThGIKqagxGsoJ7TlW/D2CAI1wP8aKSjV6oyZRF7yEUf6eLyF8KhqWrqzOSBeh+VeWECYnHa5OmP4ecSOFExXR3Pd/zZxEJoK4aAOSIXRjJSuwcVaWOJFOlrOSunj93+p0NgHmbkvwuuEdEqFNoVgICRrISQDeX3YWAkay7+ttaWwICRrISQDeX3YWAkay7+ttaWwICRrISQDeX3YXA/wPjQsND6rW9eQAAAABJRU5ErkJggg==', iconSize: [20, 20]})} 
                            position={[estate.lat, estate.lon]} 
                        />
                    )
                })}
            </MapContainer>
        )
    }
  
    useEffect(() => {
      if(document){
        setMap(true)
      }
    }, [])

    
    return {
        map,
        selected,
        renderMap,
    };
}

export default useMapView;