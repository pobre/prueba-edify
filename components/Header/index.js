import React from 'react'
import useThousand from '../hooks/useThousand'

const Header = ({ profile }) => {

    const {format} = useThousand()

    return (
        <div className="flex items-center justify-start mt-5 lg:mt-0">
            <div className="border-2 border-primary-orange rounded-full w-12 h-12  lg:w-16 lg:h-16 flex justify-center items-center cursor-pointer  group">
                <div className="bg-yellow-100 rounded-full w-8 h-8 lg:w-12 lg:h-12 flex items-center justify-center group-hover:bg-yellow-200 duration-300 transition-all">
                    <img src={profile?.avatar} alt="profile" className="w-8 lg:w-10 rounded-full" />
                </div>
            </div>

            <div className="text-primary-green ml-4 ">
                <b className="text-xl lg:text-2xl font-tiemposhl">Bienvenido de nuevo, </b> <span className="text-xl ml-1 lg:text-2xl lg:ml-2 font-tiemposhl">{profile?.name}</span> <span className="text-lg lg:text-2xl">👋</span> <br/>
                <span className="text-gray-500">Presupuesto: <b>{format(profile?.budget)}€</b></span>
            </div>
        </div>
    );
}

export default Header;