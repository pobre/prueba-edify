import React from 'react'

const useThousand = () => {

    const format = (x) => {
        return x?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }

    return {
        format
    };
}

export default useThousand;