import React from 'react'
import Sidebar from './nav/sidebar'
import Navbar from './nav/Navbar'

const Layout = ({ children, user }) => {
    return (
        <div className="grid grid-cols-12">

            <div className="hidden lg:block col-span-2">
                <Sidebar user={user} />
            </div>

            <div className="block lg:hidden col-span-12">
                <Navbar />
            </div>

            <div className="col-span-12 lg:col-span-10 bg-secondary-grey h-screen p-2 lg:p-10 overflow-auto site-container-scroll">
                {children}
            </div>
        </div>
    );
}

export default Layout;