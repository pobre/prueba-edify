import { nanoid } from 'nanoid'
import { Home, Document, Buy, Ticket, Logout, Setting, Heart } from 'react-iconly'

export const ROUTES = [
    {
        id: nanoid(),
        title: 'PROPIEDADES',
        disabled: false,
        children: [
            {
                id: nanoid(),
                title: 'Todas las propiedades',
                path: '/',
                pathname: '',
                icon: Home,
            },
            {
                id: nanoid(),
                title: 'Guardadas',
                path: '/guardadas',
                pathname: 'guardadas',
                icon: Heart
            },
        ]
    },
    {
        id: nanoid(),
        title: 'CONTENIDO',
        disabled: true,
        children: [
            {
                id: nanoid(),
                title: 'Guía Edify',
                path: 'guides',
                icon: Document
            },
            {
                id: nanoid(),
                title: 'Tienda Merch',
                path: 'marketplace',
                icon: Buy
            }
        ]
    },
    {
        id: nanoid(),
        title: 'CONTACTO',
        disabled: true,
        children: [
            {
                id: nanoid(),
                title: 'Tickets',
                path: 'all',
                icon: Ticket
            }
        ]
    }
]


export const SETTINGS = [
    {
        id: nanoid(),
        title: 'Settings',
        path: 'settings',
        disabled: true,
        icon: Setting
    },
    {
        id: nanoid(),
        title: 'Logout',
        path: 'logout',
        disabled: true,
        icon: Logout
    },
]