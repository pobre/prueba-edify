import React from 'react'
import { useRouter } from 'next/router';
import Image from 'next/image';
import Link from 'next/link';
import Logo from '../../public/img/resources/Logo/Color/Light.png'
import {ROUTES, SETTINGS} from './ROUTES.js'


const Sidebar = ({ user }) => {

    const route = useRouter()

    return (
        <nav className="w-full h-screen relative">

            <div className="p-10">
            <Image 
                    src={Logo}
                    quality={100}
                    placeholder="blur"
                    width={220}
                    height={70}
                />
            </div>

            <div>
                {ROUTES.map((nav, i)=>{
                    return (
                        <div 
                            key={nav.id} 
                            className={`
                                ${i !== 0 ? "mt-5 text-sm 2xl:text-base 2xl:mt-10" : "text-sm 2xl:text-base"}
                                ${nav.disabled ? 'opacity-20 cursor-default' : ''}
                            `}
                        >
                            <span className="pl-6 2xl:pl-10 text-gray-400 text-sm">{nav.title}</span>
                            {nav?.children.map((child)=>{
                                return(
                                    <Link href={nav.disabled ? '' : child.path}>
                                        <div key={child.id} className="flex justify-end items-center h-10 pl-4 2xl:pl-8 mt-2">
                                            <div 
                                                className={`
                                                    w-11/12 h-10 flex justify-start items-center rounded-lg pl-2 duration-300 transition-all cursor-pointer 
                                                    ${route.asPath.split('/')[route.asPath.split('/').length-1] === child.pathname ? 'text-primary-orange hover:bg-yellow-50' : 'text-gray-900 hover:bg-gray-100'}
                                                `}
                                            >
                                                <child.icon set="light" />
                                                <span className="pl-2">{child.title}</span>
                                            </div>
                                            <div className="w-1/12 flex justify-end items-center">
                                                {route.asPath.split('/')[route.asPath.split('/').length-1] === child.pathname &&
                                                    <div className="w-2 h-10 bg-primary-orange rounded-l-lg " />
                                                }
                                            </div>
                                        </div>
                                    </Link>
                                )
                            })}
                        </div>
                    )
                })}
            </div>

            <div className="absolute bottom-0 w-full h-80 text-sm 2xl:text-base p-6 2xl:p-10">

                <div className="rounded-lg bg-gray-100 p-3 flex justify-start items-center hover:bg-gray-200 duration-300 transition-all cursor-pointer mb-10">
                    <div className="w-12 h-12 bg-white rounded-full flex justify-center items-center">
                        <img src={user?.avatar} alt="profile" className="w-10 rounded-full" />
                    </div>
                    <div className="ml-2 w-2/3">
                        <div className="w-full">
                            <span className="text-gray-800">{user?.name}</span>
                        </div>
                        <div className="w-full">
                            <p className="text-gray-400 truncate">{user?.email}</p>
                        </div>
                    </div>
                </div>

                {SETTINGS.map((child)=>{
                    return(
                        <div key={child.id} className="flex justify-end items-center h-10 mt-4">
                            <div 
                                className={`w-full h-10 flex justify-start items-center rounded-lg pl-2 duration-300 transition-all cursor-pointer ${route.asPath.split('/')[route.asPath.split('/').length-1] === child.path ? 'text-blue-700 hover:bg-blue-50' : (child.path === 'logout' ? 'text-red-500 hover:bg-red-100' : 'text-gray-900 hover:bg-gray-100') }`}
                            >
                                <child.icon set="light" />
                                <span className="pl-2">{child.title}</span>
                            </div>
                        </div>
                    )
                })}

            </div>

        </nav>
    );
}

export default Sidebar;