import React, { useState } from 'react'
import Image from 'next/image';
import Link from 'next/link';
import {useRouter} from 'next/router';
import { motion, AnimatePresence } from 'framer-motion'
import { ArrowRightSquare } from 'react-iconly'
import Logo from '../../public/img/resources/Logo/Color/Light.png'
import {ROUTES} from './ROUTES.js'

const Navbar = () => {

    const route = useRouter()

    const [openMenu, setOpenMenu] = useState(false)

    return (
        <div className="bg-white">
            <div className="flex items-center justify-start p-4">
                <div className="w-1/2 flex items-center justify-start">
                    <Image 
                        src={Logo}
                        quality={100}
                        placeholder="blur"
                        width={110}
                        height={35}
                    />
                </div>
                <div onClick={()=>setOpenMenu(!openMenu)} className={`w-1/2 justify-end flex items-center cursor-pointer`}>
                    <ArrowRightSquare className={`duration-300 transition-all ${openMenu ? 'transform rotate-90' : ''}`} size={30} />
                </div>
            </div>
            <AnimatePresence>
                {openMenu &&
                    <motion.div 
                        initial={{ height: "0rem", opacity: 0}} 
                        animate={{ height: "20rem", opacity: 1 }} 
                        exit={{ height: "0rem", opacity: 0 }}
                        className="bg-white overflow-auto site-container-scroll" 
                    >
                        {ROUTES.map((nav, i)=>{
                            return (
                                <div 
                                    key={nav.id} 
                                    className={`
                                        ${i !== 0 ? "mt-5 text-sm 2xl:text-base 2xl:mt-10" : "text-sm 2xl:text-base"}
                                        ${nav.disabled ? 'opacity-20 cursor-default' : ''}
                                    `}
                                >
                                    <span className="pl-6 2xl:pl-10 text-gray-400 text-sm">{nav.title}</span>
                                    {nav?.children.map((child)=>{
                                        return(
                                            <Link href={nav.disabled ? '' : child.path}>
                                                <div key={child.id} className="flex justify-end items-center h-10 pl-4 2xl:pl-8 mt-2">
                                                    <div 
                                                        className={`
                                                            w-11/12 h-10 flex justify-start items-center rounded-lg pl-2 duration-300 transition-all cursor-pointer 
                                                            ${route.asPath.split('/')[route.asPath.split('/').length-1] === child.pathname ? 'text-primary-orange hover:bg-yellow-50' : 'text-gray-900 hover:bg-gray-100'}
                                                        `}
                                                    >
                                                        <child.icon set="light" />
                                                        <span className="pl-2">{child.title}</span>
                                                    </div>
                                                    <div className="w-1/12 flex justify-end items-center">
                                                        {route.asPath.split('/')[route.asPath.split('/').length-1] === child.pathname &&
                                                            <div className="w-2 h-10 bg-primary-orange rounded-l-lg " />
                                                        }
                                                    </div>
                                                </div>
                                            </Link>
                                        )
                                    })}
                                </div>
                            )
                        })}
                    </motion.div>
                }
            </AnimatePresence>
        </div>
    );
}

export default Navbar;