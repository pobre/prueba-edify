import React from 'react'

const Switch = ({ active, setActive, tabs}) => {
    return (
        <div className="w-full flex justify-end items-center space-x-2">
            {tabs.map((tab)=>{
                return(
                    <div onClick={()=>setActive(tab.id)} className={` rounded-xl p-3 shadow-sm cursor-pointer duration-300 transition-all ${active === tab.id ? 'bg-primary-green text-white' : 'bg-white opacity-50 hover:opacity-100'}`}>
                        <tab.icon set="light" />
                    </div>
                )
            })}
        </div>
    );
}

export default Switch;